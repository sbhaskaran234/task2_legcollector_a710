#include "LegCollectorApp.h"
#include "LegDataParser.h"
#include <iostream>

/* parses Json object from the given string */
bool LegDataParser::genJsonObjectFromString(std::string& str, Json::Value& root)
{
	return reader.parse(str, root);
}

/* this function needs to be aware of *both* the Discovery schema, and the subset schema! */
/* return false if error */
bool LegDataParser::genHourlyLegSubsetDiscData(Json::Value& orig, Json::Value& subset)
{
	/* "reset" subset */
	subset = Json::Value::null;
	/* for each node in orig node list ... */
	for (int i = 0, j = 0; i < orig.size(); i++) {
		/* iterate through elements and ... */
		/* pick only the "status" and the "uuid" entries */
		subset[i]["status"] = orig[i]["status"];
		subset[i]["uuid"] = orig[i]["uuid"];
		subset[i]["modelnum"] = orig[i]["hardware"]["modelnum"];
		subset[i]["sw_ver"] = orig[i]["software"]["firmware"];
	}

	return true;
}


/* this function needs to be aware of Leg subset schema */
/* return false if error */
bool LegDataParser::genHourlyUuidListFromLegSubsetDiscData(Json::Value& legSubset, Json::Value& uuidList, Json::Value& legOnlineSubset)
{
	/* "reset" subset */
	uuidList = Json::Value::null;
	legOnlineSubset = Json::Value::null;
	
	/* for each node in legSubset node list ... */
	for (int i = 0, j = 0; i < legSubset.size(); i++) {
		/* iterate through elements and ... */
		/* pick only the uuid's with status "Online" */
		if (legSubset[i]["modelnum"].asString() == "ARTIK-030"
			&& legSubset[i]["status"]["state"].asString() == "Online") {
			uuidList[j]["uuid"] = legSubset[i]["uuid"];
			/* also generate legOnlineSubset here */
			legOnlineSubset[j] = legSubset[i];
			j++;
		}
	}

	return true;
}

int LegDataParser::styledOutput(Json::Value jsonObj)
{
	return 0;
}

int LegDataParser::rawOutput(Json::Value jsonObj)
{
	return 0;	
}
