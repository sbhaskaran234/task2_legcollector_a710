# Makefile for Legrand Collector App

CXXFLAGS = `pkg-config --libs --cflags libartik-sdk-base libartik-sdk-connectivity`

#OBJS = $(patsubst %.c,%.o,$(wildcard *.c))
OBJS = LegCollectorApp.o LegEventGenerator.o LegCollector.o jsoncpp.o LegRestApi.o LegDataParser.o LegDb.o

legCollectorApp: $(OBJS)
	echo $(OBJS)
	g++ $(CXXFLAGS) -o $@ $^
	
%.o: %.cpp %.h
	g++ $(CXXFLAGS) -c $< -o $@

clean:
	rm -f *.o	
