#ifndef __LEG_COLLECTOR_H__
#define __LEG_COLLECTOR_H__

#include <string>
#include "json/json.h"

/* per debug 08/23/17: increase to 10 secs */
#define CUSTOM_DATA_WAIT_SECS (10) 

class LegCollector {
public:
	/* default */
	LegCollector() { };
	~LegCollector() { };

	/* enables collector, and starts the first discovery event */
	int start();
	/* disables collector */
	int stop();

	bool inHourlyCollectionCycle();
	bool doingDiscovery();
	bool doingCustomCollection();
	bool doingCustomCollectionStep();

	/* if collector enabled, starts discovery (and thus the hourly collection cycle) */
	/* reschedules itself each hourly event */
	/* sets the hourly collection cycle flag */
	/* sets the discovery flag at beginning, and resets it and end */
	/* return type is void, because it is a callback on the event queue */
	void getDiscoveryList();

	/* starts the custom collection cycle */
	/* does collection in custom collection steps */
	/* sets custom collection flag at beginning; does NOT reset it at end, since
	    last callback step will do so! */
	/* initializes all vars for multi-step engine */
	/* e.g. totalCurrentCustomSteps, currentCustomDataListIndexValue, etc. */
	/* fires off the first custom step ...*/
	int getHourlyCustomDataList();

	/* wait timeout fires, Gets Custom data from Enm via REST API */
	/* resets custom collection step flag at end */
	/* after custom collection step, increments index, and checks multi-step variables 
	    to see if need to schedule another custom step */
	/* if all steps done, does not schedule another custom step
	    and turns of custom cycle flag! */
	/* typically, schedules the next custom step */
	/* return type is void, because it is a callback on the event queue */
	void onCustomCollectionStepWaitDoneEvent_doEnmGet();

	/* concatenates received custom data to hourly custom list */
	int concatenateReceivedCustomDataToHourlyCustomList();

	/* amalgates all received data into hourlyData */
	/* this is the data that will go hourly into the DB */
	int amalgamateHourlyData();

public:
    Json::Value hourlyDiscoveredData;
    std::string hourlyDiscoveredDataStr;
    Json::Value hourlyLegSubsetDiscoveredData;
    Json::Value hourlyOnlineLegSubsetDiscoveredData;
    Json::Value hourlyUuidList;
    Json::Value hourlyCustomDataList;
    Json::Value hourlyData;
    Json::Value hubUuid;
    /* the uuid corresponding to current custom data step */
    std::string currentCustomDataUuidStr;
    Json::Value currentCustomData;

private:
	bool collectorEnabled = false;
	bool hourlyCollectionCycleActive = false;
	bool discoveryActive = false;
	bool customCollectionActive = false;
	bool customCollectionStepActive = false;
	int currentCustomDataListIndexValue = 0;
	int totalCurrentHourCustomSteps = 0;

	/* sets current uuid string, based on current index */
	/* issues REST API POST to ENM.  ENM in turn commands start of collection of 
	    custom data from appropriate UUID */
	/* schedules a "wait done" callback at the end */
	/* at each step, schedules an appropriate callback for the next step (event queue) */
	/* when done with all custom steps, resets the hourly collection cycle flag */
	int startCustomCollectionStep();


};
#endif // __LEG_COLLECTOR_H__
