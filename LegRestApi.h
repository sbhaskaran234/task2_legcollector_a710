#ifndef __LEG_REST_API_H__
#define __LEG_REST_API_H__

#include <string>

class LegRestApi {
public:
	/* sets the local discoverDataString member */
	/* uses artik SDK REST (http) api */
	void enmRequestDiscoveryData();

	/* basically communicates to ENM the schema of the custom data... */
	int enmSetCustomConfig();
	
	/* request latest collected custum data from ENM for the particular uuid */
	int enmRequestCustomData(std::string& uuid);

	/* GETs the custom data from ENM. */
	/* stuffs the currentCustomDataStr member with returned data from ENM 
	    (for that particular uuid) */
	int enmRetrieveCustomData(std::string& uuid);

	/* returns the (private) discovered data string */
	std::string getDiscoveredDataString();
	int setDiscoveredDataString(char *c_str);

	int getHubUuid();
	int getPublicIpAddr();

public:
	std::string currentCustomDataStr;
	std::string selfUuidResponseStr;
	std::string publicIpv4 = "0.0.0.0";


private:
	std::string discoveredDataString;
};

#endif // __LEG_REST_API_H__