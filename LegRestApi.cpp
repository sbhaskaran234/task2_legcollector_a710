
#include "LegCollectorApp.h"
#include <iostream>
#include <sstream>

#include <artik_module.h>
#include <artik_http.h>
#include <artik_network.h>

extern "C" {
  static artik_error artik_http_get_discovery_data();
  static artik_error artik_http_post_start_reporting(std::string& uuid);
  static artik_error artik_http_put_json_schema_spec(std::string& uuid);
  static artik_error artik_http_get_custom_data(std::string& uuid);
  static artik_error artik_http_get_self_uuid();
  static artik_error artik_net_get_current_public_ip();
}

/* sets the local discoverDataString member */
/* uses artik SDK REST (http) api */
void LegRestApi::enmRequestDiscoveryData()
{
	/* for now, ERROR printout in log ... */
	/* sets the discovered data string from within the call */
	artik_http_get_discovery_data();
}

std::string LegRestApi::getDiscoveredDataString()
{
	DebugCode(
		/* print the string first */
		std::cout << discoveredDataString << std::endl;
	);
	return discoveredDataString;
}

int LegRestApi::setDiscoveredDataString(char *c_str)
{
	discoveredDataString = c_str;
	return 0;
}

/* basically communicates to ENM the schema of the custom data... */
int LegRestApi::enmSetCustomConfig()
{
	DebugCode(
		printf("[REST_API]: [NOP for now] Placeholder Json Config at start of Custom cycle. Needs to be uuid specific.\n");
	);
	return 0;
}

/* request latest collected custum data from ENM for the particular uuid */
/* first, do a POST to start report, then do a PUT to specify json schema for response */
int LegRestApi::enmRequestCustomData(std::string& uuid)
{
	/* per debug 08/21/17: switch order of put and post, and 5 secs wait ... */
//	artik_http_post_start_reporting(uuid);
	/* also do a PUT of the Json schema for custom data here ...*/
    artik_http_put_json_schema_spec(uuid);
	artik_http_post_start_reporting(uuid);
	DebugCode(
		printf("[REST_API]: Requested Custom Data from ENM: PUTPOST ...\n");
		printf("[REST_API]: Also specified Json schema to ENM for the specific uuid.\n");
	);
	return 0;
}

/* GETs the custom data from ENM. */
/* stuffs the currentCustomDataStr member with returned data from ENM 
    (for that particular uuid) */
int LegRestApi::enmRetrieveCustomData(std::string& uuid)
{
	
	Json::Value v = Json::Value::null;
	const char *spoof = "{\"rssi\": 1, \"LeaderID\": 0x2001, \"reset60min\": 8, \"RLOC16\": 0x0123, \"OnCnt\": 1, \"OffCnt\": 1, \"DimmUpCnt\": 1, \"DimmDownCnt\": 1 }";

	/* GET the uuid-specific custom data */
	/* schema has already specified */ 
	/* currentCustomDataStr is set from within fcn! */
    artik_http_get_custom_data(uuid);

    /* currentCustomDataStr has been set from within above fcn */
	v = Json::Value(currentCustomDataStr);

	DebugCode(
		printf("[REST_API]: Retrieved Custom Data from ENM ...\n");
		std::cout << "[REST_API]: Received: " << currentCustomDataStr << std::endl;
		std::cout << "[REST_API]: Styled Obj output is: " << v.toStyledString() << std::endl;
	);
	return 0;
}


/* return 0 if success, negative if fail */
int LegRestApi::getHubUuid()
{
	int ret_val;
	Json::Value selfUuidResponseObj;

	/* this sets the selfUuidResponseStr data member */
    ret_val = artik_http_get_self_uuid();

	/* parses Json object from response string */
	if ( !legDataParserSingleton->genJsonObjectFromString(selfUuidResponseStr, selfUuidResponseObj) ) {
		fprintf(stdout, "[LegRestApi] ERROR: parsing self uuid response string\n");
	}

	/* now that we have object, get the self-uuid and set it */
	legCollectorSingleton->hubUuid = selfUuidResponseObj["uuid"];

	DebugCode(
		printf("[REST] self-uuid is %s\n", legCollectorSingleton->hubUuid.asString().c_str());
	);

	return ret_val;
}

/* return 0 if success, negative if fail */
int LegRestApi::getPublicIpAddr()
{
	int ret_val;
    ret_val = artik_net_get_current_public_ip();
	return ret_val;
}


/* uses loopback address as address of ENM! */
static artik_error artik_http_get_discovery_data()
{
	artik_http_module *http = (artik_http_module *)artik_request_api_module("http");
	artik_error ret = S_OK;
	char *response = NULL;
	artik_http_headers headers;
	artik_http_header_field fields[] = {
		{(char *)"user-agent", (char *)"Artik browser"},
		{(char *)"Accept-Language", (char *)"en-US,en;q=0.8"},
	};

	headers.fields = fields;
	headers.num_fields = sizeof(fields) / sizeof(fields[0]);

	DebugCode(
		fprintf(stdout, "artik_http_get_discovery_data: %s starting\n", __func__);
	);

	ret = http->get("http://127.0.0.1:/v1.0/devices", &headers, &response, NULL, NULL);

	if (ret != S_OK) {
		fprintf(stdout, "artik_http_get_discovery_data: ERROR: %s failed (err=%d)\n", __func__, ret);
		return ret;
	}

	if (response) {
		DebugCode(
			fprintf(stdout, "artik_http_get_discovery_data: %s response data: %s\n", __func__, response);
		);
		legRestApiSingleton->setDiscoveredDataString(response);
		free(response);
	}

	DebugCode(
		fprintf(stdout, "artik_http_get_discovery_data: %s succeeded\n", __func__);
	);

	artik_release_api_module(http);

	return ret;
}


#define POST_ACTION '{"data": {"actions":[{"name":"LegRep"}]}}'
#define POST_ACTION_ESCAPE "{\"data\": {\"actions\":[{\"name\":\"LegRep\"}]}}"

static artik_error artik_http_post_start_reporting(std::string& uuid)
{
	artik_http_module *http = (artik_http_module *)artik_request_api_module("http");
	artik_error ret = S_OK;
	char *response = NULL;
	artik_http_headers headers;
	artik_http_header_field fields[] = {
		{(char *)"user-agent", (char *)"Artik browser"},
		{(char *)"Accept-Language", (char *)"en-US,en;q=0.8"},
		// added for Legrand POST
		{(char *)"Content-Type", (char *)"application/json"},
	};
	std::ostringstream httpString;

	/* build up http string */
	httpString << "http://localhost/v1.0/devices/" << uuid  << "/messages";

	/* build up body */
	/* body is just POST_ACTION_ESCAPE */

	/* build up fields */
	/* just added a field for Legrand POST */

	/* set up fields and do an http post */

	headers.fields = fields;
	headers.num_fields = sizeof(fields) / sizeof(fields[0]);

	DebugCode(
		fprintf(stdout, "artik_http_post_start_reporting: %s starting Legrand POST\n", __func__);
	);

	/* POST_ACTION gives error, so trying POST_ACTION_ESCAPE */
	ret = http->post(httpString.str().c_str(), &headers, POST_ACTION_ESCAPE, &response, NULL, NULL);

	if (ret != S_OK) {
		fprintf(stdout, "artik_http_post_start_reporting: %s Legrand POST failed (err=%d)\n", __func__, ret);
		return ret;
	}

	if (response) {
		DebugCode(
			fprintf(stdout, "artik_http_post_start_reporting: %s Legrand POST response data: %s\n", __func__, response);
		);
		free(response);
	}

	DebugCode(
		fprintf(stdout, "artik_http_post_start_reporting: %s Legrand POST succeeded\n", __func__);
	);

	artik_release_api_module(http);

	return ret;
}

/* ENM JSON parser does not like 0x in value! */
// changed "neighbor" in below from list to int
#define JSON_SCHEMA_NEW2_SPEC_ESCAPE_NO_0x_FIX_ARRAY "{\"rssi\": 1, \"LeaderID\": 1, \"reset60min\": 8, \"RLOC16\": 1, \"OnCnt\": 1, \"OffCnt\": 1, \"DimmUpCnt\": 1, \"DimmDownCnt\": 1, \"neighbors\": 1, \"attCreate\": 1, \"succCreate\": 2, \"attRetrieve\": 3, \"succRetrieve\": 4, \"attUpdate\": 5, \"succUpdate\": 6, \"attDelete\": 7, \"succDelete\": 8, \"attNotify\": 9, \"succNotify\": 10 }"

static artik_error artik_http_put_json_schema_spec(std::string& uuid)
{
	artik_http_module *http = (artik_http_module *)artik_request_api_module("http");
	artik_error ret = S_OK;
	char *response = NULL;
	artik_http_headers headers;
	artik_http_header_field fields[] = {
		{(char *)"user-agent", (char *)"Artik browser"},
		{(char *)"Accept-Language", (char *)"en-US,en;q=0.8"},
		// added content type field for Legrand
		{(char *)"Content-Type", (char *)"application/json"},
	};
	std::ostringstream httpString;

	/* build up http string */
	httpString << "http://localhost/v1.0/devices/" << uuid  << "/customproperties";

	/* create the body for the put */
	/* body is just JSON_SCHEMA_SPEC_ESCAPE */

	/* create the appropriate header fields */
	/* just added content type field for Legrand */

	headers.fields = fields;
	headers.num_fields = sizeof(fields) / sizeof(fields[0]);

	DebugCode(
		fprintf(stdout, "artik_http_put_json_schema_spec: %s starting\n", __func__);
	);

	ret = http->put(httpString.str().c_str(), &headers, JSON_SCHEMA_NEW2_SPEC_ESCAPE_NO_0x_FIX_ARRAY, &response, NULL, NULL);

	if (ret != S_OK) {
		fprintf(stdout, "artik_http_put_json_schema_spec: %s failed (err=%d)\n", __func__, ret);
		return ret;
	}

	if (response) {
		DebugCode(
			fprintf(stdout, "artik_http_put_json_schema_spec: %s response data: %s\n", __func__, response);
		);
		free(response);
	}

	DebugCode(
		fprintf(stdout, "artik_http_put_json_schema_spec: %s succeeded\n", __func__);
	);

	artik_release_api_module(http);

	return ret;
}

/* Note: GET has no body, so content type is n/a */
static artik_error artik_http_get_custom_data(std::string& uuid)
{
	artik_http_module *http = (artik_http_module *)artik_request_api_module("http");
	artik_error ret = S_OK;
	char *response = NULL;
	artik_http_headers headers;
	artik_http_header_field fields[] = {
		{(char *)"user-agent", (char *)"Artik browser"},
		{(char *)"Accept-Language", (char *)"en-US,en;q=0.8"},
	};
	std::ostringstream httpString;

	/* build up http string */
	httpString << "http://localhost/v1.0/devices/" << uuid  << "/customproperties";

	/* create the body for the get */
	/* no body for a get */

	/* create the appropriate header fields */
	/* no content type needed for get, since no body */

	headers.fields = fields;
	headers.num_fields = sizeof(fields) / sizeof(fields[0]);

	DebugCode(
		fprintf(stdout, "artik_http_get_custom_data: %s starting\n", __func__);
	);

	ret = http->get(httpString.str().c_str(), &headers, &response, NULL, NULL);

	if (ret != S_OK) {
		fprintf(stdout, "artik_http_get_custom_data: ERROR: %s failed (err=%d)\n", __func__, ret);
		return ret;
	}

	if (response) {
		DebugCode(
			fprintf(stdout, "artik_http_get_custom_data: %s response data: %s\n", __func__, response);
		);
		/* set the rest api singleton data member */
		legRestApiSingleton->currentCustomDataStr = response;
		free(response);
	}

	DebugCode(
		fprintf(stdout, "artik_http_get_custom_data: %s succeeded\n", __func__);
	);

	artik_release_api_module(http);

	return ret;
}

/* Note: GET has no body, so content type is n/a */
/* returns 0 (S_OK) if success, negative if fail */
static artik_error artik_http_get_self_uuid()
{
	artik_http_module *http = (artik_http_module *)artik_request_api_module("http");
	artik_error ret = S_OK;
	char *response = NULL;
	artik_http_headers headers;
	artik_http_header_field fields[] = {
		{(char *)"user-agent", (char *)"Artik browser"},
		{(char *)"Accept-Language", (char *)"en-US,en;q=0.8"},
	};
	std::ostringstream httpString;

	/* build up http string */
	httpString << "http://localhost/v1.0/devices/self";

	/* create the body for the get */
	/* no body for a get */

	/* create the appropriate header fields */
	/* no content type needed for get, since no body */

	headers.fields = fields;
	headers.num_fields = sizeof(fields) / sizeof(fields[0]);

	DebugCode(
		fprintf(stdout, "artik_http_get_self_uuid: %s starting\n", __func__);
	);

	ret = http->get(httpString.str().c_str(), &headers, &response, NULL, NULL);

	if (ret != S_OK) {
		fprintf(stdout, "artik_http_get_self_uuid: ERROR: %s failed (err=%d)\n", __func__, ret);
		return ret;
	}

	if (response) {
		DebugCode(
			fprintf(stdout, "artik_http_get_self_uuid: %s response data: %s\n", __func__, response);
		);
		/* set the rest api singleton data member */
		legRestApiSingleton->selfUuidResponseStr = response;
		free(response);
	}

	DebugCode(
		fprintf(stdout, "artik_http_get_self_uuid: %s succeeded\n", __func__);
	);

	artik_release_api_module(http);

	return ret;
}

/* returns 0 (S_OK) if success, negative if fail */
static artik_error artik_net_get_current_public_ip() {
    artik_network_module *network = (artik_network_module *)artik_request_api_module("network");
    artik_error ret;
    artik_network_ip current_ip;
    DebugCode(
	    fprintf(stdout, "artik_net_get_current_public_ip: %s starting\n", __func__);
    );

    /* Check Current IP */
    ret = network->get_current_public_ip(&current_ip);
    if (ret == S_OK) {
    	/* set class member */
		legRestApiSingleton->publicIpv4 = current_ip.address;
        fprintf(stdout, "Public IP Address: %s\n", legRestApiSingleton->publicIpv4.c_str());
    } else {
        fprintf(stdout, "artik_net_get_current_public_ip: %s failed (err=%d)\n", __func__, ret);
        goto exit;
    }

    DebugCode(
	    fprintf(stdout, "artik_net_get_current_public_ip: %s succeeded\n", __func__);
    );

exit:
    artik_release_api_module(network);

    return ret;

}
