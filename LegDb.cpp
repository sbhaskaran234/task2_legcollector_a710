#include "LegCollectorApp.h"
#include <iomanip>  // std::setw() and std::setfill()
#include <sys/stat.h> // check if dir exists 

/* open DB resources (e.g. file), and init data structures */
int LegDb::start()
{
	int ret_val;

	/* initialize uninitialized member variables */
	maxCountInSegment = MAX_COUNT_IN_SEGMENT;

	/* ensure that DB dir is okay */
	ret_val = checkAndCreateDbDir(DB_DIR_NAME);
	if (ret_val < 0) {
		fprintf(stderr, "[LegDb] ERROR: Failed to set up DB dir %s correctly\n", DB_DIR_NAME);
		exit(EXIT_FAILURE);
	}

	DebugCode(
		printf("[DB] Created dir %s correctly\n", DB_DIR_NAME);
	);

	ret_val = readCountFileAndInitLocalVar();

	dbFilename = genCurDbFilename(DB_DIR_NAME, DB_FILE_ROOT_NAME, segmentCount);

	/* open in append mode */
	/* no return value; throws exception on failure */
	dbFile.open(dbFilename, std::fstream::app);
	DebugCode(
		printf("[DB]: Started DB.  Opened and initialized DB resources ...\n");
	);
	return 0;
}

/* close DB resources (e.g. file), and clean up data structures */
int LegDb::stop()
{
	int ret_val;

	ret_val = writeCountFile();

	if (ret_val < 0) {
		fprintf(stderr, "[LegDb] ERROR: Failed to write to Count file %s\n", COUNT_FILENAME);
		exit(EXIT_FAILURE);
	}
	dbFile.close();
	printf("[DB]: Stopped DB.  Closed and cleaned up DB resources ...\n");
	return 0;
}

/* Take the uuid-related discovery and multi-step custom data, and amalagamate 
    into hourly data */
/* this fcn needs to be aware of Leg subset schema, custom data schema, and 
    hourly DB data schema! */
int LegDb::amalgamateAndOrderHourlyCollectedData()
{
	DebugCode(
		printf("[DB]: Amalgamated Hourly Data...\n");
	);
	return 0;
}

/* dump hourly collected data in to DB */
int LegDb::dumpHourlyCollectedData()
{
	dbFile << legCollectorSingleton->hourlyData.toStyledString() << std::endl;
	printf("[DB]: Stored Hourly collected data into DB ...\n");

	countWithinSegment++;
	if (countWithinSegment > MAX_COUNT_IN_SEGMENT) {
		/* reset count in segment, increment segment count, close old file, gen name for 
		  new segment file and open it for append */
		segmentCount++;
		countWithinSegment = 1;
		dbFile.close();
		dbFilename = genCurDbFilename(DB_DIR_NAME, DB_FILE_ROOT_NAME, segmentCount);
		/* open in append mode */
		/* no return value; throws exception on failure */
		dbFile.open(dbFilename, std::fstream::app);
		printf("[DB]: Opened new segment file: %s\n", dbFilename.c_str() );
	}
	return 0;
}

/* returns empty string if error (test with string::empty()) */
std::string LegDb::genCurDbFilename(const char *dir, const char *rootNm, int segCount)
{
	std::ostringstream ssFileName;

	/* concatenate name with segment count */
	ssFileName << dir << "/" << rootNm << std::setw(3) << std::setfill('0') << segCount;
	return (ssFileName.str());
}

/* 0 = success; negative = fail */
int LegDb::checkAndCreateDbDir(const char *dir)
{
	int ret_val = 0;
	struct stat sb;

	/* check if dir exists, and return if it does */
	if ( stat(dir, &sb) == 0  &&  S_ISDIR(sb.st_mode) ) {
		return 0;
	} else {
		/* if doesn't exist, create it */
		printf("[DB] dir %s doesn't exist; creating ...\n", dir);
		ret_val = mkdir(dir, 0777);

		/* if create failure, then return err */
		if (ret_val < 0) return -1;
	}
	return ret_val;
}

int LegDb::rollDbFilename()
{
	return 0;
}

/* writes both count within Segment, and segment count! */
/* 0 = success; negative = fail */
int LegDb::writeCountFile()
{
	std::string fileName;

	fileName = std::string(DB_DIR_NAME) + "/" + COUNT_FILENAME;

	/* open count file for write */
	fileLeg30DayCount.open(fileName, std::ios::out);
	/* write counts into file */
	fileLeg30DayCount << countWithinSegment << std::endl; 
	fileLeg30DayCount << segmentCount << std::endl; 

	printf("[DB] Wrote counts into file %s\n", fileName.c_str() );

	/* close file */
	fileLeg30DayCount.close();
	return 0;
}

/* Read from count file, and open appropriate "tail" file */
/* read both count in segment and segment count */
/* 0 = success; negative = error */
int LegDb::readCountFileAndInitLocalVar()
{
	std::ostringstream fileName;
	int ret_val = 0;

	/* concatenate name */
	fileName << DB_DIR_NAME << "/" << COUNT_FILENAME;

	std::fstream testFile(fileName.str());
	/* open and read count file vals into local vars; 
	   if doesn't exist, initialize both counts to 1 */
	/* the writing of the file in non-existence case will be taken care of during 
	    eventual write */
	if (testFile.good()) {
		/* open file and read and stuff vars */
		fileLeg30DayCount.open(fileName.str(), std::ios::in);
		fileLeg30DayCount >> countWithinSegment;
		fileLeg30DayCount >> segmentCount;
	} else {
		/* set both counts to 1 */
		printf("[DB] Warning: count file does not exist.  Setting both countWithinSegment "
			"and segmentCount to 1\n");
		countWithinSegment  = 1;
		segmentCount = 1;
	}

	printf("[DB] Read counts from file: countWithinSegment: %d, segmentCount: %d\n",
		countWithinSegment, segmentCount);

	/* close count and test files */
	testFile.close();
	fileLeg30DayCount.close();
	return ret_val;
}

int LegDb::createAndInitCountFile()
{
	return 0;
}
