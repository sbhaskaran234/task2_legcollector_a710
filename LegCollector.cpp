#include "LegCollectorApp.h"
#include "LegCollector.h"
#include "LegEventGenerator.h"  // to enable a function to schedule itself 
							 // as callback on the event queue
#include <iostream>

/* enables collector, and starts the first discovery event */
int LegCollector::start()
{
	int ret_val;
	collectorEnabled = true;

	/* get hub uuid */
	ret_val = legRestApiSingleton->getHubUuid();

	/* exit if REST API is not properly up! */
	if (ret_val < 0) {
		fprintf(stdout, "[LegCollector] ERROR: while getting Hub UUID; REST API to ENM bad.\n");
		exit(EXIT_FAILURE);
	}

	/* get public ip address */
	legRestApiSingleton->getPublicIpAddr();

	/* starts the first discovery event */
	/* reschedules itself each hourly event */
	getDiscoveryList();
	return ret_val;
}

int LegCollector::stop()
{
	collectorEnabled = false;
	return 0;
}

bool LegCollector::inHourlyCollectionCycle()
{
	return hourlyCollectionCycleActive;
}

bool LegCollector::doingDiscovery()
{
	return discoveryActive;
}

bool LegCollector::doingCustomCollection()
{
	return customCollectionActive;
}

bool LegCollector::doingCustomCollectionStep()
{
	return customCollectionStepActive;
}

/* if collector enabled, starts discovery (and thus the hourly collection cycle) */
/* reschedules itself each hourly event */
/* sets the hourly collection cycle flag */
/* sets the discovery flag at beginning, and resets it and end */
/* after discovery, starts the custom collection cycle */
/* return type is void, because it is a callback on the event queue */

void LegCollector::getDiscoveryList()
{
	if (collectorEnabled) {
		if (!inHourlyCollectionCycle()) {
			hourlyCollectionCycleActive = true;
			discoveryActive = true;

			DebugCode(
				printf("Started hourly collection cycle.\n");
				printf("Started discovery.\n");
			);

			/* use REST API to get discovery data ... */
			/* sets the local (to Rest Api) discoverDataString member */
			/* uses artik SDK REST (http) api */
			legRestApiSingleton->enmRequestDiscoveryData();

			/* returns the (private to REST Api) discovered data string */
			hourlyDiscoveredDataStr = legRestApiSingleton->getDiscoveredDataString();

			/* parses Json object from the Leg Collector Discovered Data String */
			if ( !legDataParserSingleton->genJsonObjectFromString(hourlyDiscoveredDataStr, hourlyDiscoveredData) ) {
				fprintf(stdout, "[LegCollector] ERROR: parsing discovery data string\n");
			}

			DebugCode(
				std::cout << "Starting stylized output ..." << std::endl;
				std::cout << hourlyDiscoveredData.toStyledString() << std::endl;
			);

			/* generate Legrand subset of discovered data */
			if ( !legDataParserSingleton->genHourlyLegSubsetDiscData(hourlyDiscoveredData, hourlyLegSubsetDiscoveredData) ) {
				fprintf(stdout, "[LegCollector] ERROR: parsing Leg Subset discovery data\n");
			}

			DebugCode(
				std::cout << "Starting Leg subset discovered data ..." << std::endl;
				std::cout << hourlyLegSubsetDiscoveredData.toStyledString() << std::endl;
			);

			/* generate uuid list from Legrand subset of discovered data */
			if ( !legDataParserSingleton->genHourlyUuidListFromLegSubsetDiscData(hourlyLegSubsetDiscoveredData, hourlyUuidList, hourlyOnlineLegSubsetDiscoveredData) ) {
				fprintf(stdout, "[LegCollector] ERROR: parsing uuid list from Leg Subset discovery data\n");
			}

			DebugCode(
				std::cout << "Starting uuid list ..." << std::endl;
				std::cout << hourlyUuidList.toStyledString() << std::endl;
			);

			/* when done with discovery and associated processing, turn off discovery flag */
			discoveryActive = false;

			DebugCode(
				printf("Completed discovery.\n");
			);

			/* reschedule myself for the next hour */
			/* (it only gets scheduled if EventGenerator is enabled) */
			if (eventGeneratorSingleton) {
				eventGeneratorSingleton->scheduleCallback( [this](void){this->getDiscoveryList();}, interval_secs );
			}

			DebugCode(
				printf("Completed rescheduling for another hourly discovery.\n");
			);

			/* now start custom collection cycle */
			getHourlyCustomDataList();
		}
	}
}

/* starts the custom collection cycle */
/* does collection in custom collection steps */
/* sets custom collection flag at beginning; does NOT reset it at end, since
    last callback step will do so! */
/* initializes all vars for multi-step engine */
/* e.g. totalCurrentCustomSteps, currentCustomDataListIndexValue, etc. */
/* fires off the first custom step ...*/
int LegCollector::getHourlyCustomDataList()
{
	int ret_val = -1;
	if ( inHourlyCollectionCycle() && !doingDiscovery() ) {
		customCollectionActive = true;
		DebugCode(
			printf("[LegCollector] Started custom collection cycle.\n");
		);

		/* initialize multi-step engine variables */
		currentCustomDataListIndexValue	 = 0;
		totalCurrentHourCustomSteps = hourlyUuidList.size();
		hourlyCustomDataList = Json::Value::null;

		/* set custom config in ENM for custom data */
		ret_val = legRestApiSingleton->enmSetCustomConfig();
		
		/* start first custom data collection step */
		ret_val = startCustomCollectionStep();

	}
	return 0;
}

	/* sets custom step flag */
	/* sets current uuid string, based on current index */
	/* issues REST API POST to ENM.  ENM in turn commands start of collection of 
	    custom data from appropriate UUID */
	/* schedules a "wait done" callback at the end */
int LegCollector::startCustomCollectionStep()
{
	int ret_val;
	
	customCollectionStepActive = true;
	currentCustomDataUuidStr = hourlyUuidList[currentCustomDataListIndexValue]["uuid"].asString();
	DebugCode(
		printf("[LegCollector] Issuing POST to ENM to request custom data from Edge Node (A030)\n");
		printf("[LegCollector] uuid[%d] = %s\n", currentCustomDataListIndexValue, currentCustomDataUuidStr.c_str());
	);
	
	/* Issue a REST API post call here to get custom data */
	/* and schedule callback for 1 sec */
	
	ret_val = legRestApiSingleton->enmRequestCustomData(currentCustomDataUuidStr);

	if (eventGeneratorSingleton) {
		eventGeneratorSingleton->scheduleCallback( 
			[this](void){this->onCustomCollectionStepWaitDoneEvent_doEnmGet();}, 
			CUSTOM_DATA_WAIT_SECS );
	}
	return 0;
}

/* wait timeout fires, Gets Custom data from Enm via REST API */
/* resets custom collection step flag at end */
/* after custom collection step, increments index, and checks multi-step variables 
    to see if need to schedule another custom step */
/* if all steps done, does not schedule another custom step
    and turns of custom cycle flag! */
/* typically, schedules the next custom step */
/* return type is void, because it is a callback on the event queue */
void LegCollector::onCustomCollectionStepWaitDoneEvent_doEnmGet()
{
	int ret_val = -1;
	
	DebugCode(
		printf("[LegCollector] Issuing GET to ENM to retrieve custom data from Edge Node\n");
	);

	/* get Custom data from ENM via REST API */
	/* below stuffs the retrieved string into a member in LegRestApi singleton */
	ret_val = legRestApiSingleton->enmRetrieveCustomData(currentCustomDataUuidStr);

	/* now parse and convert the above retrieved value to a Json object */
	/* if throws error, error msg won't get printed! */
	if ( !legDataParserSingleton->genJsonObjectFromString(legRestApiSingleton->currentCustomDataStr, currentCustomData) ) {
		fprintf(stdout, "[LegCollector] ERROR: parsing custom data string\n");
	}

	DebugCode(
		std::cout << "Styled Custom Data Object: \n" << currentCustomData.toStyledString() << std::endl;
	);

	/* Concatenate received custom data to hourlyCustomDataList!! */
	concatenateReceivedCustomDataToHourlyCustomList();

	/* reset custom step flag, after getting needed data */
	customCollectionStepActive = false;
	/* increment custom step index */
	currentCustomDataListIndexValue++;
	/* if remaining steps, schedule another step, else "close down" multi-step states */
	if (currentCustomDataListIndexValue < totalCurrentHourCustomSteps) {
		/* schedule next step as a callback */
		/* since this is after a GET, it doesn't need full wait time */
		/* so, schedule callback immediately */
		if (eventGeneratorSingleton) {
			eventGeneratorSingleton->scheduleCallback( 
				[this](void){this->startCustomCollectionStep();}, 
				0 );
//				CUSTOM_DATA_WAIT_SECS );
		}
	} else {
		/* when iteration over all custom data steps is done, 
		    store hourly custom list in DB, and reset custom collection flag */
		    
		amalgamateHourlyData();

		/* store hourly custom list in DB */ 
		legDbSingleton->dumpHourlyCollectedData();
		
		customCollectionActive = false;
		/* also reset hourly collection flag here */
		hourlyCollectionCycleActive = false;
		printf("[LegCollector] Completed custom collection cycle.\n");
		printf("[LegCollector] Completed hourly collection cycle.\n");
	}
}


/* concatenates received custom data to hourly custom list */
int LegCollector::concatenateReceivedCustomDataToHourlyCustomList()
{
#ifndef NULLIFY_NON_A030_CUSTOM_DATA
	/* append custom data */
	hourlyCustomDataList[currentCustomDataListIndexValue] = currentCustomData;
#else
	/* if NOT A030, then append data, else append Json::Value::null */
 if ( hourlyOnlineLegSubsetDiscoveredData[currentCustomDataListIndexValue]["modelnum"].asString() 
 																				== "ARTIK-030" ) {
	hourlyCustomDataList[currentCustomDataListIndexValue] = currentCustomData;
} else {
	hourlyCustomDataList[currentCustomDataListIndexValue] = Json::Value::null;
}
#endif	// NULLIFY_NON_A030_CUSTOM_DATA
	DebugCode(
		printf("[LegCollector]: Concatenated received custom data to Hourly custom list ...\n");
	);
	return 0;
}

int LegCollector::amalgamateHourlyData()
{
	std::string t;
	Json::Value uuidSpecificAmalgamatedDataList = Json::Value::null;

	/* clear previous hourly data */
	hourlyData = Json::Value::null;	

	/* attach timestamp */
	t = eventGeneratorSingleton->getCurrentTimestamp();

	hourlyData["timestamp"] = Json::Value(t);

	/* also add self hub uuid, and public ipv4 */
	hourlyData["self_hub_uuid"] = hubUuid;
	hourlyData["public_ipv4"] = Json::Value(legRestApiSingleton->publicIpv4);

	DebugCode(
		std::cout << "[LegCollector]: Current time is: " << t << std::endl;
		printf("[LegCollector]: Attached timestamp to hourly list ...\n");
	);

	/* iterate through current hour uuid's and generate uuid-amalgamated list */
	for (int i = 0; i < hourlyUuidList.size(); i++) {
		/* amalgamate into uuid-specific list */
#ifndef TEST_EXCEPTION_TRAP
		uuidSpecificAmalgamatedDataList[i]["uuid"] = hourlyUuidList[i]["uuid"];
#else
		uuidSpecificAmalgamatedDataList /*[i]["uuid"]*/ = hourlyUuidList[i]["uuid"];
#endif		
		uuidSpecificAmalgamatedDataList[i]["leg_disc_subset"]["status"] = hourlyOnlineLegSubsetDiscoveredData[i]["status"];
		uuidSpecificAmalgamatedDataList[i]["leg_disc_subset"]["modelnum"] = hourlyOnlineLegSubsetDiscoveredData[i]["modelnum"];
		uuidSpecificAmalgamatedDataList[i]["leg_disc_subset"]["sw_ver"] = hourlyOnlineLegSubsetDiscoveredData[i]["sw_ver"];
		uuidSpecificAmalgamatedDataList[i]["custom_data"] = hourlyCustomDataList[i];
	}

	/* now that uuid-specific amalgamated list is done, add it to the hourly data */
	hourlyData["hourlydata"] = uuidSpecificAmalgamatedDataList;
	DebugCode(
		std::cout << "Starting hourly data ..." << std::endl;
		std::cout << hourlyData.toStyledString() << std::endl;
	);

	return 0;
}
