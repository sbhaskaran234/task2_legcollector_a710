#!/bin/bash
#  this script uploads the set of files specified on the input list
#  from the Source dir to the Destination dir
# PRECONDITIONS:
# -  ftp server is set up with correct accesses and permissions for user
# -  server has write permissions for user in appropriate directories
# -  server has overwrite file permissions for user (else uploading same name twice will fail)
# -  destination directory exists on server!
#
# Usage: ./Leg_upload.sh <file1> <file2> ... [NOTE: no dir path; only filenames]
#  e.g.: ./Leg_upload.sh LegDbFile005 LebDbFile009

# ftp: -i turns off interactive prompting; -n restrains "auto-login" from .netrc settings

VERSION="0.99"
SOURCE_DIR="/var/local/LegDb"
DEST_DIR="/uploads"
USER_ID="anonymous"
# need backslash for the anonymous password to "go through"; else prompts
PASS_PHRASE="\ "
FTP_SERVER_IP="192.168.1.21"


# cleanup previous log before starting
rm -f /tmp/ftp.log*

echo $*
for i in $*
  do
  echo $i
  /usr/bin/ftp -inv >> /tmp/ftp.log.good 2>> /tmp/ftp.log.err << END_FTP
    open $FTP_SERVER_IP
    user $USER_ID $PASS_PHRASE
    bin
    lcd $SOURCE_DIR
    cd $DEST_DIR
    put $i
    quit
# END_FTP does NOT like leading blanks!!  (bash heredoc parsing fails!)
# should actually have two leading blanks, to align
END_FTP
done

