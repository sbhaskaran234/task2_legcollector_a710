#ifndef __LEG_DATA_PARSER_H__
#define __LEG_DATA_PARSER_H__

#include "json/json.h"

class LegDataParser {
public:
	/* parses Json object from the given string */
	bool genJsonObjectFromString(std::string& str, Json::Value& root);

	/* this function needs to be aware of *both* the Discovery schema, and the subset schema! */
	/* return false if error */
	bool genHourlyLegSubsetDiscData(Json::Value& orig, Json::Value& subset);
	bool genHourlyUuidListFromLegSubsetDiscData(Json::Value& legSubset, Json::Value& uuidList, Json::Value& legOnlineSubset);
	int styledOutput(Json::Value jsonObj);
	int rawOutput(Json::Value jsonObj);

private:
	Json::Reader reader;
};

#endif // __LEG_DATA_PARSER_H__
