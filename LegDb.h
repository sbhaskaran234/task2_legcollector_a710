#ifndef __LEG_DB_H__
#define __LEG_DB_H__

#include "json/json.h"
#include <fstream>

#define DB_FILE_ROOT_NAME "LegDbFile"  //the segment num will be appended to this
#define DB_DIR_NAME "/var/local/LegDb"
#define MAX_SEGMENT_COUNT (999)  // 3 decimal digits
#define COUNT_FILENAME "Leg30DayCount"

class LegDb {
public:
	/* open DB resources (e.g. file), and init data structures */
	int start();
	/* close DB resources (e.g. file), and clean up data structures */
	int stop();
	/* Take the uuid-related discovery and multi-step custom data, and amalagamate 
	    into hourly data */
	/* this fcn needs to be aware of Leg subset schema, custom data schema, and 
	    hourly DB data schema! */
	int amalgamateAndOrderHourlyCollectedData();
	/* dump hourly collected data in to DB */
	int dumpHourlyCollectedData();

public:
	Json::Value hourlyCollectedData;

private:
	std::ofstream dbFile;
	int countWithinSegment = 0;  // set from fileLeg30DayCount
	int segmentCount = 0;  // set from fileLeg30DayCount
	int maxCountInSegment;
	/* contains both the count withint segment, as well as the segment count! */
	std::fstream fileLeg30DayCount;
	std::string dbFilename; // this is appended with segment count

private:
	/* returns empty string if error (test with string::empty()) */
	std::string genCurDbFilename(const char *dir, const char *rootNm, int segCount);
	int checkAndCreateDbDir(const char *dir);
	int rollDbFilename();
	int writeCountFile();
	int readCountFileAndInitLocalVar();
	int createAndInitCountFile();
};

#endif // __LEG_DB_H__