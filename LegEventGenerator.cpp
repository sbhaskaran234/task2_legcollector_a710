#include <list> // for runQueue
#include <thread> // for this_thread()
#include "LegEventGenerator.h"
#include "LegCollectorApp.h"
#include <iostream>
#include <time.h>  // for c time_t formatting

/* create an EventSpec instance (i.e. an event) */
EventSpec::EventSpec(const Callback& _cb, int _timeout, std::string const & _name)
{
	/* deep copy */
	cb = _cb;
	eventName = _name;
	timeout = (std::chrono::system_clock::now() + std::chrono::seconds(_timeout)).time_since_epoch().count();
}

/* enables Event Generator; allows queuing of events */
int EventGenerator::enable()
{
	_eventGeneratorEnabled = true;

	return 0;
}


/* starts main event loop, with ms resolution */
int EventGenerator::start()
{
	_eventGeneratorEnabled = true;
	try {
		/* start event loop */
		/* keeps running till async event disables event generator */
		loop();
	}
	catch (const std::exception& e) {
		std::cerr << "ERROR: caught standard exception" << std::endl;
		std::cerr << e.what() << std::endl;	
		exit(EXIT_FAILURE);
	}
	catch (...) {
		std::cerr << "ERROR: caught a non-standard exception" << std::endl;
		exit(EXIT_FAILURE);
	}
	return 0;
}

/* sets flag to exit main loop appropriately */
int EventGenerator::stop()
{
	_eventGeneratorEnabled = false;
	return 0;
}

bool EventGenerator::eventGeneratorEnabled()
{
	return _eventGeneratorEnabled;
}

void EventGenerator::scheduleCallback(const Callback& cb, int timeout, std::string const & eventName)
{
	if (eventGeneratorEnabled() ) {
		std::lock_guard<std::recursive_mutex> ev_lock(m_EventQueueLock);
		m_EventQueuePending.push_back(EventSpec(cb, timeout, eventName));
	}
}

/* gets the current timestamp in <buffer, 26, "%Y:%m:%d %H:%M:%S"> format */
/*
 * time_point is essentially just a duration from a clock-specific epoch.
 * 
 * If you have a std::chrono::system_clock::time_point, then you can use std::chrono::system_clock::to_time_t 
 * to convert the time_point to a time_t, and then use the normal C functions such as ctime 
 * or strftime to format it.
 */
std::string EventGenerator::getCurrentTimestamp()
{
	char buffer[26];
	struct tm *pTimeValues;

	std::chrono::system_clock::time_point cur_tp = std::chrono::system_clock::now();
	std::time_t curTime = std::chrono::system_clock::to_time_t(cur_tp);
	/* now that we have a time_t, use couple of c functions to get correct format */
	pTimeValues = localtime(&curTime);
	/* converts to string */
	strftime(buffer, 26, "%Y:%m:%d %H:%M:%S", pTimeValues);
	return std::string(buffer);
}


void EventGenerator::loop()
{
	std::list<EventSpec> runQueue;

	while (eventGeneratorEnabled() ) {
		/* Parse through the event list and check for any timeout events */
		/* put block braces below, so  mutex automatically is releases (destroyed) at end of block */
		{
			std::lock_guard<std::recursive_mutex> ev_lock(m_EventQueueLock);
			for (int i = 0, size = m_EventQueuePending.size(); i < size;) {
				if (m_EventQueuePending[i].timeout <= std::chrono::system_clock::now().time_since_epoch().count()) {
					runQueue.push_back(std::move(m_EventQueuePending[i]));
					m_EventQueuePending.erase(m_EventQueuePending.begin()+i);
					--size;
				} else {
					++i;
				}
			}
		}

		while (!runQueue.empty() && eventGeneratorEnabled() ) {
			/* call the callback, and pop */
			runQueue.front().cb();
			runQueue.pop_front();
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(LOOP_MSEC));
	}

	/* 
	 * When it exits loop here (b/c async event):
	 *   -  appropriately "due" events have been run, but
	 *   -  pending events are still in queue.
	 */

	/* runQueue is automatically cleaned up (deleted) here, when loop exits */
}

