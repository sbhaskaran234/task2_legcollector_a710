#ifndef __LEG_COLLECTOR_APP_H__
#define __LEG_COLLECTOR_APP_H__

#include <stdio.h> // provide printf is across modules
#include "LegEventGenerator.h"
#include "LegCollector.h"
#include "LegRestApi.h"
#include "LegDataParser.h"
#include "LegDb.h"

#define SW_VERSION "1.12"
#define LEG_DEBUG
#define NULLIFY_NON_A030_CUSTOM_DATA
//#define TEST_EXCEPTION_TRAP
//#define DEMO_MODE_DB_CIRCULATE

#ifndef DEMO_MODE_DB_CIRCULATE
  #define MAX_COUNT_IN_SEGMENT (24 * 30)  // 24 hrs per day * 30 days/month
#else
  #define MAX_COUNT_IN_SEGMENT (2)
#endif // DEMO_MODE_DB_CIRCULATE

#if defined(LEG_DEBUG)
  #define DebugCode( code_fragment )  { code_fragment }
#else
  #define DebugCode( code_fragment )
#endif

/* this comes from command-line parameter (secs); else default 1 hr */
extern int interval_secs;
extern EventGenerator *eventGeneratorSingleton;
extern LegCollector *legCollectorSingleton;
extern LegRestApi *legRestApiSingleton;
extern LegDataParser *legDataParserSingleton;
extern LegDb *legDbSingleton;

#endif // __LEG_COLLECTOR_APP_H__
