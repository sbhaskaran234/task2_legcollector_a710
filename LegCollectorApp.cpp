/* 
 * App to collect Legrand network performance data. 
 *  Done hourly, and stored in a databasek
 *  for eventual upload to an ftp server (separate bash script).
 *
 * The main modules are: an EventGenerator, and a Collector.
 * The Event generator generates the hourly events, and the Collector 
 *  does the hourly collection, when triggered.
 * The App uses getopt() to parse input data, and sigaction() to end gracefully.
 *
 * The main() invokes the Event Generator and the Collector classes.  Both
 *  are essentially (conceptually) singletons, with no inheritance 
 *  or multiple instances.  This should explain some of the (cpp) simplifications
 *  used.  These "singletons" are deleted at the end of main(), upon exit.
 * 
 *  Also, instead of contstructors and destructors, explicit start() and stop() fcns are 
 *  used, to give more "control" over timing of clean up, etc.
 *
 *  Also, there is a combination of C and C++ code, since the REST API was first
 *    implemented using C libraries, and it was decided to keep the working C code,
 *    while also using the additional C++ libraries needed for full functionality.
 *
 *  Also, decided not to use exceptions, per Google C++ Style Guide
 *
 * The Legrand Collector uses the REST API to query data from ENM, or to 
 *  command the Edge nodes (e.g. A030) via the ENM.
 *
 * Couple of features have been added:
 *  -  public ipv4 associated with self hub uuid
 *  -  "circulate" the database every 30 days (720 hours)
 *      a new segment is started after 30 days.
 *  -  separate shell script that will upload collected data to an ftp server
 *
 * Error Handling:
 *  -  unknown exception is caught, printed, and the program exits with error code
 *  -  during the start() fcn of the Leg collector singleton:
 *      -  the self-uuid is used as a test for the REST API to the ENM
 *      -  if this fails, then the program aborts, and suggests possible fault
 *           in REST API to ENM
 *  -  other errors are printed as errors on console (stdout)
 *
 * Debug:
 *  -  there is also a debug flag in LegCollectorApp.h, which can be turned on
 *       for verbose output and debug info.
 */

#include <signal.h>
#include <stdio.h>
#include <unistd.h> // for sleep(), getopt(), etc...
#include <stdlib.h> // for atoi()
#include <getopt.h>
#include <string.h> // for memset() and strsignal()
#include "LegCollectorApp.h"


#define DEFAULT_INTERVAL_SECS ( 60 * 60 )  // one hour

/* below are global Singletons.  They are created at beginning of main(),
    and destroyed at end of main(), and remain throughout program */ 
/* may consider encapsulation, but for now, "occam's razor", and "law of parsimony" */
int interval_secs = DEFAULT_INTERVAL_SECS;
EventGenerator *eventGeneratorSingleton = NULL;
LegCollector *legCollectorSingleton = NULL;
LegRestApi * legRestApiSingleton = NULL;
LegDataParser * legDataParserSingleton = NULL;
LegDb *legDbSingleton = NULL;


/* This structure mirrors the one found in /usr/include/asm/ucontext.h */
typedef struct _sig_ucontext {
	unsigned long     uc_flags;
	struct ucontext   *uc_link;
	stack_t           uc_stack;
	struct sigcontext uc_mcontext;
	sigset_t          uc_sigmask;
} sig_ucontext_t;

static void sigint_handler(int s)
{
	/* turn off "enable" flags in event generator and collector! */
	if (eventGeneratorSingleton) {
		eventGeneratorSingleton->stop();
	}

	if (legCollectorSingleton) {
		legCollectorSingleton->stop();
	}

    printf("Disabled Event Generator and Collector.\n");
}

static void crit_err_hdlr(int sig_num, siginfo_t * info, void * ucontext)
{
	/* put appropriate easy logger stack trace command here */
	//el::base::debug::StackTrace();
	printf("Received segmentation fault signal; possibly uncaught exception? future: show backtrace.\n");
	exit(EXIT_FAILURE);
}

/* for example, to stack trace an uncaught exception, which typically 
    signals a SIGSEGV */
static void register_segfault_handler()
{
	struct sigaction sigact;

	memset(&sigact, 0, sizeof(sigact));
	sigact.sa_sigaction = crit_err_hdlr;
	sigact.sa_flags = SA_RESTART | SA_SIGINFO;

	if (sigaction(SIGSEGV, &sigact, (struct sigaction *)NULL) != 0)
	{
		fprintf(stderr, "Error setting signal handler for %d (%s)\n",
			SIGSEGV, strsignal(SIGSEGV));
	}
}


int main(int argc, char **argv)
{
	int opt;

	signal (SIGINT, sigint_handler);
	register_segfault_handler();

#ifdef TEST_EXCEPTION_TRAP
//	int *pInt = NULL;

//	*pInt = 5;
#endif	

	while ( (opt = getopt(argc, argv, "i:hv")) != -1 ) {
		switch(opt){
		case 'i':
			interval_secs = atoi(optarg);
			break;
		case 'v':
			printf("Legrand Network Collector, version %s.\n", SW_VERSION);
			return 0;
			break;
		case 'h':
		default:
			printf("Usage: legCollectorApp [-i <discovery interval in secs>] - (default is 1 hour)\n");
			printf("                       [-v] show version \n"); 
			printf("                       [-h] show this help \n"); 
			return 0;
		}
	}

	printf("Starting Legrand Network Collector, version %s.\n", SW_VERSION);
	printf("Discover interval set to %d secs.\n", interval_secs);

	/* now that options are parsed, start the Event Generator and Collector instances */
	/* only instantiation here; nothing done in constructors (since default) */
	eventGeneratorSingleton = new EventGenerator();
	legCollectorSingleton = new LegCollector();
	legRestApiSingleton = new LegRestApi();
	legDataParserSingleton = new LegDataParser();
	legDbSingleton = new LegDb();

	/* enables Event Generator; allows queuing of events */
	eventGeneratorSingleton->enable();
	/* open DB resources, etc. */
	legDbSingleton->start();
	/* starts the first discovery, and queues itself */
	legCollectorSingleton->start();
	/* this starts the event loop; never returns, till async event */
	/* the event loop is started here, after the other relevant components are "readied" (e.g. start()) */
	eventGeneratorSingleton->start();

	/* if got here, then collector and event generator are already stopped by async (ctrl-C) event. */
	/* delete event generator and Legrand collector here! */
	/* stop() fcn should automatically clean up any event queues!
	    And also buffers (e.g. json buffers!) */
	if (eventGeneratorSingleton) {
		eventGeneratorSingleton->stop();
	}

	if (legCollectorSingleton) {
		legCollectorSingleton->stop();
	}

	/* close DB resources, free related data structures, etc. */
	legDbSingleton->stop();

	/* only deletions here (since default destructors); the stop() earlier does cleanup */
	delete eventGeneratorSingleton;
	delete legCollectorSingleton;
	delete legDbSingleton;
	delete legRestApiSingleton;
	delete legDataParserSingleton;
	
	return 0;
}