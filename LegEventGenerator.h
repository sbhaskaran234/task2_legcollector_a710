#ifndef __LEG_EVENT_GENERATOR_H__
#define __LEG_EVENT_GENERATOR_H__

#include <string>
#include <mutex> // for mutex
#include <vector>

#define LOOP_MSEC (1)

/* prototype for callback */
typedef std::function<void()> Callback;


struct EventSpec {
	Callback cb;
	std::string eventName;
	unsigned long long timeout;

	/* constructor */
	EventSpec(const Callback& _cb, int _timeout, std::string const & _name = "");
};


class EventGenerator {
public:
	/* use implicit (default) constructor and deconstructor */

	/* enables Event Generator; allows queuing of events */
	int enable();
	/* starts main event loop, with ms resolution */
	int start();
	/* sets flag to exit main loop appropriately */
	int stop();
	bool eventGeneratorEnabled();
	void scheduleCallback(const Callback& cb, int timeout = 0, std::string const & eventName = "");
	
	std::string getCurrentTimestamp();

private:
	void loop();

	bool _eventGeneratorEnabled = false;

	std::vector<EventSpec>  m_EventQueuePending;
	std::recursive_mutex  m_EventQueueLock;

};

#endif // __LEG_EVENT_GENERATOR_H__
