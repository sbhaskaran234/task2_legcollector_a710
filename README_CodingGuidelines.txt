Some Coding Guidelines used:
-  camel variable names for C++
-  underscore variable names for C (e.g. this_is_a_var_name)
-  "pick and choose" from Google C++ Style Guide
	-  e.g. minimal use of exceptions, printf usage as appropriate, etc.
-  Prefix module names with LegXXX (for Legrand)
-  macro names all capitalized
-  tabs = 4
-  "law of parsimony"/Occam's razor (i.e. keep it simple...)